package method_dril;

public class Practice {
	public static void main(String[] args) {

		// 設問1
		// 処理内容：”Hello"という文字を出力する
		System.out.println("設問1");
		printHello();

		// 設問2
		// 処理内容：円周率(3.141592...)の値を出力する
		System.out.println("設問2");
		printPI();

		// 設問3
		// 処理内容："こんばんは","こんにちは","おはよう"の3つのあいさつからランダムに1つを表示する
		System.out.println("設問3");
		printRandomMessage();
		System.out.println();

		// 設問4
		// 処理内容：引数で渡されたメッセージを出力する
		System.out.println("設問4");
		String q4 = "テストメッセージ";
		printMessage(q4);

		// 設問5
		// 処理内容：引数で渡された値を半径とする円の面積を出力する
		System.out.println("設問5");
		double q5 = 2.5;
		printCircleArea(q5);

		// 設問6
		// 処理内容："こんばんはxxさん","こんにちはxxさん","おはようxxさん"の３つのあいさつからランダムに1つ表示する
		//				   あいさつ文のxxのところには引数で渡されたnameの文字が入る
		System.out.println("設問6");
		String q6 = "福田";
		printRandomMessage(q6);

		// 設問7
		// 処理内容：文字列 message を、 count の回数だけ繰り返し出力する
		System.out.println("設問7");
		String q7str = "あいうえお";
		int q7int = 3;
		printMessage(q7str, q7int);

		// 設問8
		// 処理内容：高さ height, 横幅が width の長方形の面積を出力する
		System.out.println("設問8");
		double q8double1 = 1.5;
		double q8double2 = 1.5;
		printRectangleArea(q8double1, q8double2);

		// 設問9
		// 処理内容：引数で渡された a, b, c の値のうち、もっとも大きな値を出力する
		System.out.println("設問9");
		double q9double1 = 1.1;
		double q9double2 = 2.2;
		double q9double3 = 3.3;
		printMaxValue(q9double1, q9double2, q9double3);

		// 設問10
		// 処理内容："よろしくおねがいします"という文字列を返す
		System.out.println("設問10");
		System.out.println(getMessage());

		// 設問11
		// 処理内容：天気予報メッセージをランダムに生成して、そのメッセージを返す。
		//				  天気予報メッセージは、次の中からランダムに組み合わせて作り出すものとする。
		// 				  {今日・明日・明後日}の天気は{晴れ・曇り・雨・雪}でしょう。
		System.out.println("設問11");
		System.out.println(getWeatherForecast());

		// 設問12
		// 処理内容：2の平方根(√2)を返す
		System.out.println("設問12");
		System.out.println(getSquareRootOf2());

		// 設問13
		// 処理内容："こんばんは xx さん", "こんにちは xx さん", "おはよう xx さん" の 3 つの
		//				　あいさつからランダムに選んだ 1 つを戻り値とする。
		//				　あいさつ文の xx のところには、引数で渡された name の文字が入る
		System.out.println("設問13");
		String q13 = "福田";
		System.out.println(getRandomMessage(q13));

		// 設問14
		// 処理内容：引数で渡された value の値の絶対値を返す。
		System.out.println("設問14");
		double q14 = -5.2;
		System.out.println(getAbsoluteValue(q14));

		// 設問15
		// 処理内容：引数で渡された値が偶数の場合は true、そうでない場合は false を返す
		System.out.println("設問15");
		int q15 = 15;
		System.out.println(isEvenNumber(q15));

		// 設問16
		// 処理内容：引数で受け取る 2 の値のうち、小さい方の値を返す
		System.out.println("設問16");
		double q16double1 = 1.1;
		double q16double2 = 2.2;
		System.out.println(getMinValue(q16double1, q16double2));

		// 設問17
		// 処理内容：引数で受け取る 2 の値の絶対値が等しければ true, そうでなければ false を返す
		System.out.println("設問17");
		int q17int1 = 1;
		int q17int2 = -1;
		System.out.println(isSameAbsoluteValue(q17int1, q17int2));

		// 設問18
		// 処理内容：isKid の値が true なら、"こんにちは。xx ちゃん。" isKid の値が
		//				　false なら "こんにちは。xx さん。" という文字列を返す。ただし xx には name
		//				　の値が入る。
		System.out.println("設問18");
		String q18str = "福田";
		boolean q18bool = false;
		System.out.println(getMessage(q18str, q18bool));

		// 設問19
		// 処理内容：引数で受け取る配列の要素のうち、最も小さい値を返す
		System.out.println("設問19");
		int[] q19 = {5,3,1,2,4};
		System.out.println(getMinValue(q19));

		// 設問20
		// 処理内容：引数で受け取る配列の要素の平均値を返す
		System.out.println("設問20");
		double[] q20 = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6};
		System.out.println(getAverage(q20));

		// 設問21
		// 処理内容：引数で受け取る配列の要素のうち、最も文字数の大きい文字列を返す
		//				　文字数が同じものが複数存在する場合は、配列の後ろの方の要素を優先する
		System.out.println("設問21");
		String[] q21 = {"1","22","333","4444","55555","44445","3332"};
		System.out.println(getLongestString(q21));

		// 設問22
		// 処理内容：引数で受け取る Point オブジェクトの、原点からの距離を返す
		System.out.println("設問22");
		Point q22 = new Point(3.0,4.0);
		System.out.println(getDistanceFromOrigin(q22));

		// 設問23
		// 処理内容：引数で受け取る 2 つの Point オブジェクト間の距離を返す
		System.out.println("設問23");
		Point q23Point1 = new Point(1.5,2.5);
		Point q23Point2 = new Point(3.5,4.5);
		System.out.println(getDistanceBetweenTwoPoints(q23Point1, q23Point2));

		// 設問24
		// 処理内容：引数で受け取る配列に含まれる Point オブジェクトの重心座標を表す Point オブジェクト
		System.out.println("設問24");
		Point q24Point1 = new Point(1.5,2.5);
		Point q24Point2 = new Point(3.5,4.5);
		Point q24Point3 = new Point(5.5,6.5);
		Point[] q24PointArray = {q24Point1, q24Point2, q24Point3};
		Point q24Ans = getBarycenter(q24PointArray);
		System.out.println("(" + q24Ans.x + "," + q24Ans.y + ")");

		// 設問25
		// 処理内容："こんにちは xx さん" というメッセージを出力する。xx には引数列：で渡された Person オブジェクトの名前(name)を入れる。
		System.out.println("設問25");
		Person q25 = new Person("福田",26);
		printMessage(q25);

		// 設問26
		// 処理内容：引数で渡された Person オブジェクトの年齢(age)の値が 20 以上なら true, そうでないなら false を返す。
		System.out.println("設問26");
		Person q26 = new Person("福田",26);
		System.out.println(isAdult(q26));

		// 設問27
		// 処理内容：引数で渡された Person オブジェクトの年齢(age)の値が 20 以上なら true, そうでないなら false を返す。
		System.out.println("設問27");
		Person q27Person1 = new Person("福田",26);
		Person q27Person2 = new Person("草地",28);
		Person q27Person3 = new Person("小嶋",23);
		Person q27Person4 = new Person("金子",24);
		Person q27Person5 = new Person("ハンナ",26);
		Person q27Person6 = new Person("岡崎",26);
		Person q27Person7 = new Person("野呂",27);
		Person q27Person8 = new Person("福田(トモ)",23);
		Person q27Person9 = new Person("安藤",38);
		Person[] q27Array = {q27Person1, q27Person2, q27Person3, q27Person4, q27Person5, q27Person6, q27Person7, q27Person8, q27Person9};
		System.out.println(getYoungestPerson(q27Array).getName());

	}

	private static Person getYoungestPerson(Person[] persons) {
		int age = persons[0].getAge();
		int youngestIndex = 0;
		for (int i = 0; i < persons.length - 1; i++) {
			if ( persons[i].getAge() > persons[i+1].getAge() && age > persons[i+1].getAge() ) {
				youngestIndex = i+1;
			}
		}
		return persons[youngestIndex];
	}

	private static boolean isAdult(Person person) {
		if(person.getAge() >= 20) {
			return true;
		} else {
			return false;
		}
	}

	private static void printMessage(Person person) {
		System.out.println("こんにちは " + person.getName() + " さん");
	}

	private static Point getBarycenter(Point[] points) {
		double xSum = 0,ySum = 0;
		for (int i =0; i < points.length; i++) {
			xSum += points[i].x;
			ySum += points[i].y;
		}
		Point retPoint = new Point(xSum/points.length, ySum/points.length);
		return retPoint;
	}

	private static double getDistanceBetweenTwoPoints(Point p0, Point p1) {
		return Math.sqrt( (p0.x - p1.x)*(p0.x - p1.x)+(p0.y-p1.y)*(p0.y-p1.y) );
	}

	private static double getDistanceFromOrigin(Point p) {
		Point origin = new Point(0,0);
		return getDistanceBetweenTwoPoints(origin, p);
	}

	private static String getLongestString(String[] array) {
		String longestString = "";
		for (int i = 0; i < array.length - 1; i++) {
			if ( array[i].length() <= array[i+1].length() ) {
				longestString = array[i+1];
			}
		}
		return longestString;
	}

	private static double getAverage(double[] array) {
		double sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		return sum / array.length;
	}

	private static int getMinValue(int[] array) {
		int minValue = array[0];
		for (int i = 0; i < array.length - 1; i++) {
			if ( array[i] > array[i+1] && minValue > array[i+1] ) {
				minValue = array[i+1];
			}
		}
		return minValue;
	}

	private static String getMessage(String name, boolean isKid) {
		if( isKid ) {
			return "こんにちは。"+name+"ちゃん。";
		} else {
			return "こんにちは。"+name+"さん。";
		}
	}

	private static boolean isSameAbsoluteValue(int i, int j) {
		if ( Math.abs(i) == Math.abs(j) ) {
			return true;
		} else {
			return false;
		}
	}

	private static double getMinValue(double a, double b) {
		if ( a>b ) {
			return b;
		} else {
			return a;
		}
	}

	private static boolean isEvenNumber(int value) {
		if ( value % 2 == 0) {
			return true;
		} else {
			return false;
		}
	}

	private static double getAbsoluteValue(double value) {
		return Math.abs(value);
	}

	private static String getRandomMessage(String name) {
		int n = (int)(3 * Math.random());
		switch(n) {
		case 0:
			return "こんばんは"+name+"さん";
		case 1:
			return "こんにちは"+name+"さん";
		case 2:
			return "おはよう"+name+"さん";
		default:
			return "error";
		}
	}

	private static double getSquareRootOf2() {
		return Math.sqrt(2.0);
	}

	private static String getWeatherForecast() {
		String day = null;
		String weather = null;
		int n = (int)(3 * Math.random());
		switch(n) {
		case 0:
			day = "今日";
			break;
		case 1:
			day = "明日";
			break;
		case 2:
			day = "明後日";
			break;
		}
		int m = (int)(4 * Math.random());
		switch(m) {
		case 0:
			weather = "晴れ";
			break;
		case 1:
			weather = "曇り";
			break;
		case 2:
			weather = "雨";
			break;
		case 3:
			weather = "雪";
			break;
		}
		return day+"の天気は"+weather+"でしょう。";
	}

	private static String getMessage() {
		return "よろしくおねがいします";
	}

	private static void printMaxValue(double a, double b, double c) {
		if ( a > b ) {
			if ( a > c ) {
				System.out.println(a);
			} else /* c > a */ {
				System.out.println(c);
			}
		} else /* b > a */ {
			if ( b > c ) {
				System.out.println(b);
			} else /* c > b */ {
				System.out.println(c);
			}
		}
	}

	private static void printRectangleArea(double height, double width) {
		System.out.println(height * width);
	}

	private static void printMessage(String message, int count) {
		for (int i = 0; i < count; i++) {
			printMessage(message);
		}
	}

	private static void printRandomMessage(String name) {
		printRandomMessage();
		System.out.println(name+"さん");
	}

	private static void printCircleArea(double radius) {
		System.out.println(radius * 2 * Math.PI);
	}

	private static void printMessage(String message) {
		System.out.println(message);
	}

	private static void printRandomMessage() {
		int n = (int)(3 * Math.random());
		switch(n) {
		case 0:
			System.out.print("こんばんは");
			break;
		case 1:
			System.out.print("こんにちは");
			break;
		case 2:
			System.out.print("おはよう");
			break;
		}
	}

	private static void printPI() {
		System.out.println(Math.PI);
	}

	private static void printHello() {
		System.out.println("Hello");
	}
}
class Point {
	double x;
	double y;
	Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
}
class Person {
	private String name;
	private int age;
	Person(String name, int age) {
		this.name = name;
		this.age = age;
	}
	String getName() {
		return name;
	}
	int getAge() {
		return age;
	}
	// 設問28
	// 処理内容：インスタンス変数 name の値を引数の文字列に設定する
	void setName(String name) {
		this.name = name;
	}
	// 設問29
	// 処理内容：インスタンス変数 age の値を引数の値に設定する。ただし引数の値がマイナスの場合は何もしない
	void setAge(int age) {
		if (age < 0) return;
		this.age = age;
	}
	// 設問30
	// 処理内容：引数で渡された Person オブジェクトの年齢と、自分自身の年齢が同じであれば true、そうでなければ false を返す。
	boolean isSameAge(Person person) {
		int myAge = 26;
		if ( person.getAge() == myAge ) {
			return true;
		} else {
			return false;
		}
	}
}